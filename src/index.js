import express from 'express';

const app = express();
app.get('/', (req, res) => {
	const fullname = req.query.fullname;
	const separateName = fullname.split(' ');
	let result = '';
	if (separateName.length > 3) {
		result = 'Invalid fullname';
	} else {
		result = separateName.reverse().map((cur, i) => {
			let partName = cur.charAt(0).toUpperCase() + cur.slice(1);
			return i === 0 ? partName : partName.slice(0,1) + '.';
		}).join(' ');
	}
	res.send(result);
});

app.listen(3000)